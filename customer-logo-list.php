<?php

/**
 * Plugin Name:       Customer Logo List
 * Plugin URI:        https://gitlab.com/Robson16/customer-logo-list
 * Description:       A block for Customers logo list with cool effects.
 * Requires at least: 6.1
 * Requires PHP:      7.0
 * Version:           1.0.0
 * Author:            Robson H. Rodrigues
 * Author URI:        https://www.linkedin.com/in/robson-h-rodrigues-93341746/
 * License:           MIT
 * Text Domain:       customer-logo-list
 * Domain Path:       /languages
 *
 * @package           create-block
 */

function cll_customer_logo_list_block_init()
{
	register_block_type(__DIR__ . '/build');

	wp_set_script_translations(
		'cll-customer-logo-list-editor-script',
		'customer-logo-list',
		plugin_dir_path(__FILE__) . 'languages'
	);

	wp_set_script_translations(
		'cll-customer-logo-list-save-script',
		'customer-logo-list',
		plugin_dir_path(__FILE__) . 'languages'
	);
}
add_action('init', 'cll_customer_logo_list_block_init');
