��          �       �       �   2   �      0     L     _     k     w          �     �     �     �  .   �  8     �  E  @     #   S     w     �     �     �     �     �     �  #   �       .   '  8   V   A block for Customers logo list with cool effects. Add a caption to %dº image Customer Logo List Edit Images Element Tag Heading Heading options Robson H. Rodrigues Select Images Select some images first. Select the images https://gitlab.com/Robson16/customer-logo-list https://www.linkedin.com/in/robson-h-rodrigues-93341746/ Project-Id-Version: Customer Logo List
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2023-05-26 19:23+0000
PO-Revision-Date: 2023-05-29 20:14+0000
Last-Translator: Agência B&amp;B
Language-Team: Português do Brasil
Language: pt_BR
Plural-Forms: nplurals=2; plural=n != 1;
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Loco https://localise.biz/
X-Loco-Version: 2.6.4; wp-6.2.2
X-Domain: customer-logo-list Um bloco para lista de logotipos de clientes com efeitos legais. Adicione uma legenda à imagem %dº Lista de logotipos Editar imagens Marca de elemento Título Opções do Título Robson H. Rodrigues Selecionar imagens Selecione algumas imagens primeiro. Selecione as imagens https://gitlab.com/Robson16/customer-logo-list https://www.linkedin.com/in/robson-h-rodrigues-93341746/ 