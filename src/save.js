import { RichText, useBlockProps } from '@wordpress/block-editor';
import { Fragment } from '@wordpress/element';
import { __, sprintf } from '@wordpress/i18n';
import classNames from 'classnames';

export default function save( { attributes } ) {
	const {
		title,
		titleFontSize,
		titleTagName,
		images,
		backgroundColor,
		textColor,
	} = attributes;

	const TitleTagName = titleTagName;

	const blockProps = useBlockProps.save( {
		style: {
			backgroundColor: 'transparent !important',
		},
	} );

	const clientsClasses = classNames( {
		clients: true,
		'has-background': backgroundColor,
		[ `has-${ backgroundColor }-background-color` ]: backgroundColor,
		'has-text-color': textColor,
		[ `has-${ textColor }-color` ]: textColor,
	} );

	const logosClasses = classNames( {
		logos: true,
		'has-background': backgroundColor,
		[ `has-${ backgroundColor }-background-color` ]: backgroundColor,
	} );

	return (
		<div { ...blockProps }>
			{ images.length ? (
				<Fragment>
					<div className={ clientsClasses }>
						<TitleTagName style={ { fontSize: titleFontSize } }>
							<RichText.Content value={ title } />
						</TitleTagName>
						<ul>
							{ images.map( ( image, index ) => (
								<li
									key={ image.id }
									data-target-image={ image.id }
								>
									{ image.caption
										? image.caption
										: sprintf(
												// translators: %d: warning to add caption to image at this list position
												__(
													'Add a caption to %dº image',
													'customer-logo-list'
												),
												index + 1
										  ) }
								</li>
							) ) }
						</ul>
					</div>
					<div className={ logosClasses }>
						{ images.map( ( image ) => (
							<img
								key={ image.id }
								id={ image.id }
								src={ image.url }
								alt={ image.alt }
							/>
						) ) }
					</div>
				</Fragment>
			) : (
				<span className="empty-message">
					{ __( 'Select some images first.', 'customer-logo-list' ) }
				</span>
			) }
		</div>
	);
}
