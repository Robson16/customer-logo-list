import domReady from '@wordpress/dom-ready';

domReady( function () {
	const clientsList = Array.from(
		document.querySelectorAll(
			'.wp-block-cll-customer-logo-list .clients ul'
		)
	);

	const clients = Array.from(
		document.querySelectorAll(
			'.wp-block-cll-customer-logo-list .clients li'
		)
	);

	const logos = Array.from(
		document.querySelectorAll(
			'.wp-block-cll-customer-logo-list .logos img'
		)
	);

	function findLogo( logoId ) {
		return logos.find( ( logo ) => logo.id === logoId );
	}

	clientsList.forEach( ( list ) => {
		list.addEventListener( 'mouseenter', ( event ) => {
			const firstClient = event.currentTarget.querySelector( 'li' );
			const target = firstClient.dataset.targetImage;
			const targetLogo = findLogo( target );

			targetLogo.style.display = 'none';
		} );

		list.addEventListener( 'mouseleave', ( event ) => {
			const firstClient = event.currentTarget.querySelector( 'li' );
			const target = firstClient.dataset.targetImage;
			const targetLogo = findLogo( target );

			targetLogo.style.display = 'block';
		} );
	} );

	clients.forEach( ( client ) => {
		client.addEventListener( 'mouseenter', ( event ) => {
			const targetLogo = findLogo(
				event.currentTarget.dataset.targetImage
			);

			targetLogo.style.display = 'block';
		} );

		client.addEventListener( 'mouseleave', ( event ) => {
			const targetLogo = findLogo(
				event.currentTarget.dataset.targetImage
			);

			targetLogo.style.display = 'none';
		} );
	} );
} );
