import { RichText, useBlockProps } from '@wordpress/block-editor';
import {
	InspectorControls,
	MediaUpload,
	MediaUploadCheck,
} from '@wordpress/blockEditor';
import {
	Button,
	FontSizePicker,
	PanelBody,
	SelectControl,
} from '@wordpress/components';
import { Fragment, useEffect, useState, useRef } from '@wordpress/element';
import { __, sprintf } from '@wordpress/i18n';
import classNames from 'classnames';
import './editor.scss';

export default function Edit( { attributes, setAttributes } ) {
	const {
		title,
		titleFontSize,
		titleTagName,
		images,
		backgroundColor,
		textColor,
	} = attributes;

	const [ clientsClasses, setClientsClasses ] = useState( {} );
	const [ logosClasses, setLogosClasses ] = useState( {} );

	const imagesRef = useRef( [] );

	const blockProps = useBlockProps();

	const titleTagNamesOptions = [
		'h1',
		'h2',
		'h3',
		'h4',
		'h5',
		'h6',
		'p',
		'span',
	];
	const fontSizes = [
		{
			name: 'Small',
			slug: 'small',
			size: '0.875rem',
		},
		{
			name: 'Normal',
			slug: 'normal',
			size: '1rem',
		},
		{
			name: 'Medium',
			slug: 'medium',
			size: '1.5rem',
		},
		{
			name: 'Large',
			slug: 'large',
			size: 'clamp(1.65rem, 1.3077rem + 1.9231vw, 2.5rem)',
		},
		{
			name: 'X-Large',
			slug: 'x-large',
			size: 'clamp(1.75rem, -1.0962rem + 7.2115vw, 3.375rem)',
		},
		{
			name: 'XX-Large',
			slug: 'xx-large',
			size: 'clamp(1.85rem, -2.4808rem + 11.0577vw, 4.375rem);',
		},
	];

	useEffect( () => {
		setClientsClasses(
			classNames( {
				clients: true,
				'has-background': backgroundColor,
				[ `has-${ backgroundColor }-background-color` ]:
					backgroundColor,
				'has-text-color': textColor,
				[ `has-${ textColor }-color` ]: textColor,
			} )
		);

		setLogosClasses(
			classNames( {
				logos: true,
				'has-background': backgroundColor,
				[ `has-${ backgroundColor }-background-color` ]:
					backgroundColor,
			} )
		);
	}, [ clientsClasses, logosClasses, textColor, backgroundColor ] );

	return (
		<Fragment>
			<InspectorControls>
				<Fragment>
					<PanelBody
						title={ __(
							'Select the images',
							'customer-logo-list'
						) }
						initialOpen={ true }
					>
						<div className="editor-select-images">
							<MediaUploadCheck>
								<MediaUpload
									multiple={ true }
									gallery={ true }
									addToGallery={ true }
									onSelect={ ( media ) => {
										setAttributes( {
											images: media,
										} );
									} }
									allowedTypes={ [ 'image' ] }
									value={ images.map( ( item ) => item.id ) }
									render={ ( { open } ) => {
										return (
											<Fragment>
												<Button
													variant="primary"
													onClick={ ( event ) => {
														event.stopPropagation();
														open();
													} }
												>
													{ images.length > 0
														? __(
																'Edit Images',
																'customer-logo-list'
														  )
														: __(
																'Select Images',
																'customer-logo-list'
														  ) }
												</Button>
											</Fragment>
										);
									} }
								/>
							</MediaUploadCheck>
						</div>
					</PanelBody>
					<PanelBody
						title={ __( 'Heading options', 'customer-logo-list' ) }
						initialOpen={ true }
					>
						<FontSizePicker
							fontSizes={ fontSizes }
							value={ titleFontSize }
							fallbackFontSize={ 16 }
							onChange={ ( newFontSize ) =>
								setAttributes( { titleFontSize: newFontSize } )
							}
						/>
						<SelectControl
							label={ __( 'Element Tag', 'customer-logo-list' ) }
							value={ titleTagName }
							options={ titleTagNamesOptions.map(
								( titleTagNamesOption ) => {
									return {
										label: titleTagNamesOption.toUpperCase(),
										value: titleTagNamesOption,
									};
								}
							) }
							onChange={ ( newTag ) =>
								setAttributes( { titleTagName: newTag } )
							}
							__nextHasNoMarginBottom
						/>
					</PanelBody>
				</Fragment>
			</InspectorControls>

			<div { ...blockProps }>
				{ images.length ? (
					<Fragment>
						<div className={ clientsClasses }>
							<RichText
								tagName={ titleTagName }
								value={ title }
								allowedFormats={ [
									'core/bold',
									'core/italic',
								] }
								onChange={ ( newText ) =>
									setAttributes( { title: newText } )
								}
								placeholder={ __(
									'Heading',
									'customer-logo-list'
								) }
								style={ { fontSize: titleFontSize } }
							/>

							<ul
								onMouseEnter={ () => {
									imagesRef[ 0 ].style.display = 'none';
								} }
								onMouseLeave={ () => {
									imagesRef[ 0 ].style.display = 'block';
								} }
							>
								{ images.map( ( image, index ) => (
									<li
										key={ image.id }
										onMouseEnter={ () => {
											imagesRef[ index ].style.display =
												'block';
										} }
										onMouseLeave={ () => {
											imagesRef[ index ].style.display =
												'none';
										} }
									>
										{ image.caption
											? image.caption
											: sprintf(
													// translators: %d: warning to add caption to image at this list position
													__(
														'Add a caption to %dº image',
														'customer-logo-list'
													),
													index + 1
											  ) }
									</li>
								) ) }
							</ul>
						</div>
						<div className={ logosClasses }>
							{ images.map( ( image, index ) => (
								<img
									key={ image.id }
									ref={ ( image ) =>
										( imagesRef[ index ] = image )
									}
									id={ image.id }
									src={ image.url }
									alt={ image.alt }
								/>
							) ) }
						</div>
					</Fragment>
				) : (
					<span className="empty-message">
						{ __(
							'Select some images first.',
							'customer-logo-list'
						) }
					</span>
				) }
			</div>
		</Fragment>
	);
}
