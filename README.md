# Customer Logo List

- Contributors:      Robson H. Rodrigues
- Tags:              block
- Tested up to:      6.1
- Stable tag:        1.0.0
- License:           MIT

A block for Customers logo list with cool effects.

## 🛠 Technologies
This project was developed with the following technologies

- [WordPress](https://br.wordpress.org/)
- [Node.js](https://nodejs.org/)
- [React.js](https://pt-br.reactjs.org/)
- [SASS](https://sass-lang.com/)

## :floppy_disk: Installation

1. Upload the plugin files to the `/wp-content/plugins/customer-logo-list` directory, or install the plugin through the WordPress plugins screen directly.

2. Activate the plugin through the 'Plugins' screen in WordPress.

## :closed_book: Getting Started

You need to have [Node.js](https://nodejs.org/) installed, then you can clone and run this project on your WordPress installation with the instructions below:

Below you will find some information on how to run scripts.

## 👉  `npm start`
- Use to compile and run the block in development mode.
- Watches for any changes and reports back any errors in your code.

## 👉  `npm run build`
- Use to build production code for your block inside `build` folder.
- Runs once and reports back the gzip file sizes of the produced code.

## 👉  `npm format`
- It helps to enforce coding style guidelines for your files (JavaScript, YAML) by formatting source code in a consistent way.

## 👉  `npm lint:css`
- Helps enforce coding style guidelines for your style files.

## 👉  `npm lint:js`
- Helps enforce coding style guidelines for your JavaScript files.

---

### ☕❤

[Robson H. Rodrigues](https://www.linkedin.com/in/robson-h-rodrigues-93341746/)
